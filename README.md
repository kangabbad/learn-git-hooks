# learn-git-hooks

Setting up Husky pre-commit hook with ESLint, Prettier and lint-staged for React Native

### Installing packages

```bash
npm i -D eslint babel-eslint eslint-plugin-react husky lint-staged
```

### Setup .eslintrc.js

Create a `.eslintrc.js` file if it doesn't already exist

```javascript
module.exports = {
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "plugins": ["react"],
  "env": {
    "es6": true,
    "node": true
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "settings": {
    "react": {
      "pragma": "React",
      "version": "detect"
    }
  },
  "rules": {
    "max-len": [1, 1200, 2, { "ignoreComments": true }],
    "no-console":[1],
    "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
    "react/jsx-uses-vars": [2],
    "no-loop-func":[1],
    "indent": ["error", 2],
    "linebreak-style": ["error", "unix"],
    "quotes": ["error", "single"],
    "semi": ["error", "never"],
    "object-curly-spacing": ["error", "always"]
  }
}
```

### Setup prettier (optional)

Create a `.prettierrc.js` file if it doesn't already exist

```javascript
module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
}
```

### Setup pre-commit (if git hooks not working)
- Open directory `{your-root-project}/.git/hooks`
- Copy paste `pre-commit.sample` and rename file to `pre-commit`
- Add path `export PATH=/usr/local/bin:$PATH` to the beginning file

### Setup husky and lint-staged in package.json
```json
{
    ...
    "husky": {
        "hooks": {
            "pre-commit": "lint-staged"
        }
    },
    "lint-staged": {
        "*.{js,jsx}": [
            "eslint"
        ]
    }
}
```
